import React from 'react';
import styled from "@emotion/styled";

const AboutContainer = styled.div({
  textAlign: "center",
  padding: "0 1rem",
});

export default function About() {
  return (
    <AboutContainer>
      Hi, I am Vijayakumar Ravi, aka linuxnerd 🤓. I am 23 old guy from India, 
      who love Computers and Softwares. I am also a Computer Science Student and a Linux lover.
      You can follow me on{" "}
      <a href="https://github.com/vijaysrv">Gitlab.com</a> or{" "}
      <a href="https://twitter.com/intent/user?screen_name=_Vijayakumar_">
        Twitter
      </a>
      .
    </AboutContainer>
  );
}

import React, { useCallback } from "react";
import { Root, Routes } from "react-static";
import { Router, useLocation, globalHistory } from "@reach/router";

import "./styles/main.scss";

import styled from "@emotion/styled";
import { Head } from "react-static";

const RootContainer = styled.div({
  padding: "2rem",
  width: "100%",
});

const Container = styled.div({
  margin: "auto",
  maxWidth: "1100px",
  width: "100%",
  marginBottom: "20vh",
});

const DESCRIPTION =
  "The blog, exobrain, digital garden, personal musings and thoughts of me, Vijay.";

function gtag(..._args: unknown[]) {
  if (typeof window === "undefined") {
    return;
  }

  ((window as any).dataLayer = (window as any).dataLayer || []).push(arguments);
}

gtag("js", new Date());
gtag("config", "UA-153493405-1");

import { MutableRefObject, useEffect, useRef } from "react";
import { DetailToggle } from "components/DetailToggle";

export function useScrollBehaviour() {
  const location = useLocation();
  const historyState: MutableRefObject<{ [key: string]: number }> = useRef({});

  useEffect(() => {
    return globalHistory.listen(({ location, action }) => {
      if (action === "PUSH") {
        historyState.current[location.pathname] = 0;
        window.scrollTo(0, 0);
      }

      if (action === "POP") {
        const scroll = historyState.current[location.pathname] ?? 0;
        window.scrollTo(0, scroll);
      }
    });
  }, []);

  useEffect(() => {
    function scrollHandler() {
      historyState.current[location.pathname] = window.scrollY;
    }

    window.addEventListener("scroll", scrollHandler);
    return () => window.removeEventListener("scroll", scrollHandler);
  }, [location.pathname]);
}

function RouteHead() {
  const { pathname } = useLocation();

  useScrollBehaviour();

  return (
    <Head>
      <link rel="canonical" href={`https://linuxnerd.xyz${pathname}`} />
      <meta name="og:url" content={`https://linuxnerd.xyz${pathname}`} />
    </Head>
  );
}

export default function App() {
  return (
    <Root>
      <RootContainer>
        <Container>
          <Head>
            <title>The Linuxnerd Journal</title>

            <meta name="description" content={DESCRIPTION} />
            <meta name="twitter:description" content={DESCRIPTION} />

            <meta name="og:title" content="The Linuxnerd Journal" />
            <meta name="twitter:title" content="Bennett's Rust Journal" />

            <meta name="twitter:creator" content="@_Vijayakumar_" />

            <meta name="twitter:site" content="@_Vijayakumar_" />

            <meta
              name="og:image"
              content={"https://linuxnerd.xyz/profile.jpg"}
            />
            <meta
              name="twitter:image"
              content={"https://linuxnerd.xyz/profile.jpg"}
            />
            <meta name="og:image:width" content={"400"} />

            <meta name="og:image:height" content={"400"} />

            <meta name="og:type" content="article" />

            <script
              async
              src="https://www.googletagmanager.com/gtag/js?id=UA-153493405-1"
            />
          </Head>
          <React.Suspense fallback={<em>Loading...</em>}>
            <Router>
              <Routes
                path="*"
                render={useCallback(
                  ({ routePath, getComponentForPath }) => (
                    <>
                      <RouteHead />
                      {getComponentForPath(routePath)}
                    </>
                  ),
                  []
                )}
              />
            </Router>
            <DetailToggle />
          </React.Suspense>
        </Container>
      </RootContainer>
    </Root>
  );
}

My speedy personal website and digital garden.

Built with:
- [react-static]
- [emotion]

Hosted at: [linuxnerd.xyz](https://linuxnerd.xyz)

[emotion]: https://emotion.sh/docs/introduction
[react-static]: https://github.com/react-static/react-static

---
title: Good speculation
type: garden
status: seed
---

Speculative generality is bad right? I wonder what kind of speculation is good.

- Speculating that something will need to be deleted in the future?
- Speculating that something will need to be refactored?

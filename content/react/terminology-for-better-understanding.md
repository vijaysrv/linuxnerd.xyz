---
title: React terminology for better understanding
type: garden
status: seed
---

We seem to be missing an explicit shared vocabulary when it comes to React concepts.

## Ideas

- Incidental render
  - When a component renders because the parent rendered, not because of a state update
- Render
  - When a component renders because of a state change

## Examples

> This component requires an incidental render to actually update. You should change it to react to state changes

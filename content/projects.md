---
title: "My projects"
description: "A list of projects that I've worked on over the years."
type: garden
---

Just a few public projects that I've worked on over the year. The rest are on the cutting room floor.

## [Recoil Clone]

A Recoil clone written in under 100 lines. I created this for the [Rewriting Recoil from scratch](./recoil-from-scratch) post.

## [Simple Dev Blog Zola Theme]

A Zola theme that is meant to replace simple Gatsby blogs by prerendering links and including all the basics needed for SEO. It used to be the theme powering this blog.

## [Rust OBS Wrapper]

A safe Rust wrapper around the OBS API. I use this to creating OBS sources, filters, transitions and effects.

## [Darknet.js]

A Node.js wrapper of [pjreddie''s](https://pjreddie.com/) open source neural network framework Darknet.

## [Gruvbox GTK]

A simple GTK theme based on [Arc] but with Gruvbox colours. I've also created the matching [icon theme];

## [Dotfiles]

An ongoing attempt to make myself an ultra-productive Linux user.
I use my dotfiles to store my theme as well as useful scripts that I've built over the years.

[Gruvbox GTK]: https://github.com/bennetthardwick/gruvbox-gtk
[Arc]: https://github.com/horst3180/arc-theme
[icon theme]: https://github.com/bennetthardwick/gruvbox-arc-icon-theme
[Dotfiles]: https://github.com/bennetthardwick/dotfiles
[Darknet.js]: https://github.com/bennetthardwick/darknet.js
[Rust OBS Wrapper]: https://github.com/bennetthardwick/rust-obs-plugins
[Simple Dev Blog Zola Theme]: https://simple-dev-blog-zola-starter.netlify.app/
[Recoil Clone]: https://github.com/bennetthardwick/recoil-clone
